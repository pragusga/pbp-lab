from django.db import models


class Note(models.Model):
    to = models.CharField(max_length=250)
    title = models.CharField(max_length=250)
    message = models.TextField()
    note_from = models.CharField(max_length=250)

    def __str__(self):
        return self.to
