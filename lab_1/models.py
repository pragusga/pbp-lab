from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10, unique=True)
    dob = models.DateField()

    def __str__(self):
        return self.name
