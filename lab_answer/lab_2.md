# 1. Apakah perbedaan antara JSON dan XML?

- JSON adalah bahasa meta, sedangkan XML adalah bahasa markup
- JSON dapat mengandung array, sedangkan XML tidak
- JSON tidak dapat berisi komentar, sedangkan XML bisa berisi komentar
- JSON hanya men-support UTF-8 encoding, sedangkan XML dapat men-support berbagai jenis encoding
- JSON dibuat berdasatkan pada bahasa JavaScript, sedangkan XML didasarkan pada SGML (Standard Generalized Markup Language)
- XML lebih aman daripada JSON
- JSON lebih mudah dibaca daripada XML

# 2. Apakah perbedaan antara HTML dan XML?

- XML berfokus pada transfer data, sedangkan HTML berfokus pada penyajian data
- Tag XML dapat di-cutomisasi, sedangkan HTML tidak bisa di-customisasi
- XML bersifat case sensitive, sedangkan HTML bersifat case insensitive
- XML tidak menghiraukan error, sedangkan HTML tidak menampilkan error
- XML bersifat dinamis, sedangkan HTML bersifat statis
- Pada XML kita bisa menggunakan whitespace sesuka kita, sedangkan paha HTML apabila terdapat lebih dari satu whitespace yang berurutan maka hanya akan dibaca satu whitespace saja
