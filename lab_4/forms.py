from lab_2.models import Note
from django.forms import ModelForm, TextInput, NumberInput, DateTimeInput


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        labels = {
            "title": "Title",
            "to": "To",
            "from": "From",
            "message": "Message",
        }

    def __init__(self, *args, **kwargs):
        super(NoteForm, self).__init__(*args, **kwargs)

        for _, field in self.fields.items():
            field.widget.attrs.update(
                {"class": "form-control", "id": field.label}
            )
