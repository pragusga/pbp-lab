from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm
from lab_2.models import Note
from django.contrib.auth.decorators import login_required


def index(request):
    notes = Note.objects.all()
    context = {"notes": notes}
    return render(request, "lab_4/lab4_index.html", context)


def note_list(request):
    notes = Note.objects.all()
    context = {"notes": notes}
    return render(request, "lab_4/lab4_note_list.html", context)


# @login_required(login_url="/admin/login/")
def add_note(request):
    form = NoteForm()

    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("index_lab4")

    context = {"form": form}
    return render(request, "lab_4/lab4_form.html", context)
