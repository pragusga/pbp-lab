from lab_1.models import Friend
from django.forms import ModelForm, TextInput, NumberInput, DateTimeInput


class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        labels = {"name": "Nama", "npm": "NPM", "dob": "Tanggal Lahir"}
        widgets = {
            "name:": TextInput(attrs={"class": "input input__name"}),
            "npm": NumberInput(
                attrs={
                    "class": "input input__npm",
                    "minlength": 10,
                }
            ),
            "dob": DateTimeInput(
                attrs={"class": "input input__dob", "type": "date"}
            ),
        }
        error_messages = {
            "name": {"required": "Nama tidak boleh kosong"},
            "npm": {
                "required": "NPM tidak boleh kosong",
                "invalid": "NPM tidak valid",
                "max_length": "NPM tidak boleh lebih dari 10 angka",
            },
            "dob": {
                "required": "Tanggal Lahir tidak boleh kosong",
                "invalid": "Format tanggal salah",
            },
        }
