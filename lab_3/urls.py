from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path("", index, name="index_lab3"),
    path("add", add_friend, name="add_lab3"),
]
